/*jslint browser: true*/
/*global $, jQuery, alert*/

function onSignIn(googleUser) {
    "use strict";
    var profile = googleUser.getBasicProfile();
    $(".g-signin2").css("display", "none");
    $(".data").css("display", "block");
    $("#pic").attr('src', profile.getImageUrl());
    $("#email").text(profile.getEmail());
}

function signOut() {
    "use strict";
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
        
        alert("You have been sucessfully signed out");
        
        $(".g-signin2").css("display", "block");
        $(".data").css("display", "none");
    });
}